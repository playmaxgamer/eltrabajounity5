﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Electro;

public class NewBehaviourScript : MonoBehaviour
{
    public Text InputEscr;
    // Start is called before the first frame update
    void Start()
    {
        Lavanderia();
    }

    // Update is called once per frame
    void Update()
    {
        //Lavanderia();
    }

    public void Lavanderia()
    {
        Debug.Log("Selecciona un catalogo: ");
        Debug.Log("1.Herramienta");
        Debug.Log("2.Lavado");
        string catalogo = InputEscr.text;

        switch (catalogo)
        {
            case "1":
                Herramienta herra = new Herramienta();
                break;
            case "2":
                Lavado lava = new Lavado();
                break;
            default:
                Debug.Log("¡¡¡ERROR!!!");
                //Console.ReadKey();
                break;
        }
    }
}
