﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Electro;

namespace Electrodomesticos
{
    class Program
    {
        static void Main(string[] args)
        {
            Lavanderia();
        }

        static void Lavanderia()
        {
            Console.WriteLine("Selecciona un catalogo: ");
            Console.WriteLine("1.Herramienta");
            Console.WriteLine("2.Lavado");
            string catalogo = Console.ReadLine();

            switch (catalogo)
            {
                case "1":
                    Herramienta herra = new Herramienta();
                    break;
                case "2":
                    Lavado lava = new Lavado();
                    break;
                default:
                    Console.WriteLine("¡¡¡ERROR!!!");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
